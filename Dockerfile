# base docker image
FROM openjdk:8
LABEL maintainer="com.account"
ADD target/personal-account-0.0.1-SNAPSHOT.jar personal-account-docker.jar
ENTRYPOINT ["java", "-jar", "personal-account-docker.jar"]