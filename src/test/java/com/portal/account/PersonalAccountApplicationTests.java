package com.portal.account;

import com.portal.account.controller.PersonalAccountController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PersonalAccountApplicationTests {

   @Autowired
   private PersonalAccountController controller;

   @Test
   void contextLoads() {
       assertThat(controller).isNotNull();
   }
}
