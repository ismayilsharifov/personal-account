package com.portal.account;

import com.portal.account.controller.PersonalAccountController;
import com.portal.account.model.response.OriginalFileResponseDto;
import com.portal.account.model.response.FileResponseDto;
import com.portal.account.service.FileService;
import org.aspectj.lang.annotation.Before;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PersonalAccountControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    PersonalAccountController personalAccountController;

    @Mock
    private FileService fileService;

    @Before("")
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(personalAccountController)
                .build();
    }

    @Test
    void testFileUploaded_verifyStatus() throws Exception {
        String text = "Mock text content";
        MockMultipartFile file = new MockMultipartFile(
                "uploaded-fFile",
                "file-mock",
                MediaType.TEXT_PLAIN_VALUE,
                "This is the file content".getBytes());
        mockMvc.perform(MockMvcRequestBuilders
                        .multipart("/api/account/file_upload")
                        .file(file)
                        .param(text))
                .andExpect(status().is(200));
    }

    @Test
    void getListOriginalFiles_verifyStatus() throws Exception {
        List<FileResponseDto> files = Collections.singletonList(
                FileResponseDto.builder()
                        .id("1")
                        .text("mock-content")
                        .name("uploaded-file")
                        .uri("/api/account/files")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .size("file data".getBytes().length).build());

        Mockito.when(fileService.getAllOriginalFiles()).thenReturn(files);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/account/files"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getFile_verify_getById() throws Exception {
        OriginalFileResponseDto originalFileResponseDto =
                OriginalFileResponseDto.builder()
                        .id("1")
                        .text("mock-content")
                        .name("uploaded-file")
                        .uri("/api/account/files")
                        .type("application/xml")
                        .receiptDate(LocalDateTime.now())
                        .data("file data".getBytes())
                        .size("file data".getBytes().length).build();
        Mockito.when(fileService.getOriginalFileById("1")).thenReturn(originalFileResponseDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/files/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text", Matchers.is("mock-content")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("uploaded-file")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.uri", Matchers.is("/file/original_files/1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type", Matchers.is("application/xml")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", Matchers.is("file data".getBytes())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size", Matchers.is("file data".getBytes().length)));
        Mockito.verify(fileService).getOriginalFileById("1");
    }

    @Test
    void deleteOriginalFile_verifyStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/files/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isAccepted());
    }

}
