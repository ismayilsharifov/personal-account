package com.portal.account.exception.exceptions;

public class FIleStorageException extends RuntimeException{

    public FIleStorageException() {
    }

    public FIleStorageException(String message) {
        super(message);
    }

    public FIleStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public FIleStorageException(Throwable cause) {
        super(cause);
    }
}
