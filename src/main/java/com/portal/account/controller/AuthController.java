package com.portal.account.controller;

import com.portal.account.security.payload.request.LoginRequest;
import com.portal.account.security.payload.request.SignupRequest;
import com.portal.account.security.payload.response.AuthResponse;
import com.portal.account.security.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        AuthResponse authResponse = authService.authenticateUser(loginRequest);
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, authResponse.getResponseCookie().toString())
                .body(authResponse.getUserInfoResponse());
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return ResponseEntity.ok(authService.registerUser(signUpRequest));
    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        AuthResponse authResponse = authService.logoutUser();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, authResponse.getResponseCookie().toString())
                .body(authResponse.getMessageResponse());
    }
}