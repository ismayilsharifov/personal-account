package com.portal.account.controller;

import com.portal.account.model.response.FileMessage;
import com.portal.account.model.response.OriginalFileResponseDto;
import com.portal.account.model.response.FileResponseDto;
import com.portal.account.model.response.ResponseMessage;
import com.portal.account.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
@RestController
@Secured("ROLE_USER")
@RequestMapping("/api/account")
@RequiredArgsConstructor
public class PersonalAccountController {

    private final FileService fileService;

    @PostMapping("/file_upload")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("text") String text, @RequestParam("file") MultipartFile file) {
        try {
            FileResponseDto fileResponseDto = fileService.storeFile(text, file);
            String message = FileMessage.builder().fileId(fileResponseDto.getId())
                    .fileName(fileResponseDto.getName()).build().getUploadedMessage();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(fileResponseDto, message));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(FileMessage.NOT_UPLOADED));
        }
    }

    @GetMapping("/files")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<FileResponseDto>> getListOriginalFiles() {
        return ResponseEntity.status(HttpStatus.OK).body(fileService.getAllOriginalFiles());
    }

    @GetMapping("/files/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<byte[]> getOriginalFile(@PathVariable String id) {
        OriginalFileResponseDto responseDto = fileService.getOriginalFileById(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename= "
                        + responseDto.getName())
                .body(responseDto.getData());
    }

    @DeleteMapping("/files/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<String> deleteOriginalFile(@PathVariable String id) {
        fileService.deleteOriginalFile(id);
        String message = FileMessage.builder().fileId(id)
                .build().getDeletedMessage();
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

    @RequestMapping("/about")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ModelAndView upload(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("uploader");
        return modelAndView;
    }

}
