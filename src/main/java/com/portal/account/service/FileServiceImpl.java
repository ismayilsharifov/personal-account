package com.portal.account.service;

import com.portal.account.dao.entity.OriginalFile;
import com.portal.account.dao.repository.OriginalFileRepository;
import com.portal.account.exception.exceptions.MyFileNotFoundException;
import com.portal.account.mapper.FileMapper;
import com.portal.account.model.response.OriginalFileResponseDto;
import com.portal.account.model.response.FileResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements com.portal.account.service.FileService {
    private final OriginalFileRepository originalFileRepository;

    private final com.portal.account.service.ConvertingService convertingService;

    private final FileMapper fileMapper;

    @Override
    public FileResponseDto storeFile(String text, MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        OriginalFile originalFile = new OriginalFile(fileName, text, file.getContentType(), file.getBytes());
        originalFile = originalFileRepository.save(originalFile);
        return convertingService.makeOriginalResponse(originalFile);
    }

    @Override
    public List<FileResponseDto> getAllOriginalFiles() {
        return originalFileRepository.findAll().stream().map(
                        convertingService::makeOriginalResponse)
                .collect(Collectors.toList());
    }

    @Override
    public OriginalFileResponseDto getOriginalFileById(String id) {
        OriginalFile file = originalFileRepository.findById(id).orElseThrow(() ->
                new MyFileNotFoundException("File not found with parameter {" + id + "}"));
        return fileMapper.mapOriginalFileResponse(file);
    }


    @Override
    public void deleteOriginalFile(String id) {
        originalFileRepository.findById(id).orElseThrow(() ->
                new MyFileNotFoundException("File not found with id : " + id));
        originalFileRepository.deleteById(id);
    }

}
