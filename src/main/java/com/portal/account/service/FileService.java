package com.portal.account.service;

import com.portal.account.model.response.OriginalFileResponseDto;
import com.portal.account.model.response.FileResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface FileService {

    FileResponseDto storeFile(String text, MultipartFile xslFile) throws IOException;

    OriginalFileResponseDto getOriginalFileById(String id);


    List<FileResponseDto> getAllOriginalFiles();

    void deleteOriginalFile(String id);

}
