package com.portal.account.service;

import com.portal.account.dao.entity.OriginalFile;
import com.portal.account.model.response.FileResponseDto;
import com.portal.account.util.Utility;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConvertingService {
    public FileResponseDto makeOriginalResponse(OriginalFile file) {
        String fileDownloadUri = Utility.getOrgDownloadUri(file.getId());
        return FileResponseDto.builder()
                .id(file.getId())
                .text(file.getText())
                .name(file.getName())
                .uri(fileDownloadUri)
                .type(file.getType())
                .receiptDate(file.getReceiptDate())
                .size(file.getData().length).build();
    }
}
