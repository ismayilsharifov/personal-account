package com.portal.account.mapper;

import com.portal.account.dao.entity.OriginalFile;
import com.portal.account.model.response.OriginalFileResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface FileMapper {
    OriginalFileResponseDto mapOriginalFileResponse(OriginalFile originalFile);

   }
