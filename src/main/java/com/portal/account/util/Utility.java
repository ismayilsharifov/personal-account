package com.portal.account.util;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class Utility {
    public static String getOrgDownloadUri(String fileId){
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/api/account/")
                .path("files/")
                .path(fileId)
                .toUriString();
    }
}
