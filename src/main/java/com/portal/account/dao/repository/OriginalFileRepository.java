package com.portal.account.dao.repository;

import com.portal.account.dao.entity.OriginalFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OriginalFileRepository extends JpaRepository<OriginalFile, String> {
}
