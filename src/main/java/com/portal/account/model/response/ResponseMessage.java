package com.portal.account.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseMessage {

    private FileResponseDto responseDto;
    private String message;

    public ResponseMessage(String message) {
        this.message = message;
    }
}