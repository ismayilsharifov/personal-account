package com.portal.account.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FileMessage {
    private String fileId;
    private String fileName;
    public static String NOT_UPLOADED = "Could not upload the files";
    public String getUploadedMessage(){
        return "Uploaded the file (" + this.fileName +
                ") successfully with id : " + this.fileId;
    }
    public String getDeletedMessage(){
        return "Deleted file with id : " + this.fileId;
    }
}
