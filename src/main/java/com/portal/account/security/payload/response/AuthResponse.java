package com.portal.account.security.payload.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseCookie;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {
    private ResponseCookie responseCookie;
    private UserInfoResponse userInfoResponse;
    private MessageResponse messageResponse;
}
