package com.portal.account.security.dao.repository;

import com.portal.account.security.dao.entity.ERole;
import com.portal.account.security.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}