<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
  <title>Login page</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    body {font-family: Arial, Helvetica, sans-serif;}
    form {border: 3px solid #f1f1f1;}

    input[type=text], input[type=password] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      box-sizing: border-box;
    }

    button {
      background-color: #4CAF50;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      cursor: pointer;
      width: 100%;
    }

    button:hover {
      opacity: 0.8;
    }


    .imgcontainer {
      text-align: center;
      margin: 24px 0 12px 0;
    }

    .container {
      padding: 16px;
    }


  </style>
</head>
<body>

<h2>Error</h2>

<div class="contact section-pad">
  <div class="container">
    <div class="row">
      <div class="contact-wrapper row">
        <div class="col-md-3 col-2"></div>
        <div class="message offset-lg-2 col-lg-8 col-md-8 md-4">
          <div class="message-group"> <br/><br/><br/>
            An error occured. Please try again later.<br/>
            <a href="/">Return main page</a>
          </div>
        </div>
        <div class="col-md-3 col-2"></div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
