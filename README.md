A "personal-account" project built with Spring Boot


**Description API:**

- Sign-up Register user (Registration - Authorization with JWT and HttpOnly Cookie).

- Sign-in Authenticate user (Security Login with JWT).

- Sign-out.

- File upload: Import and store file with text content to database.

- Fetch all files.

- File download: Fetch one file.

- Delete an original file.



**Features:**

- A client can upload a file with text content for storing to the database.

- A client can view all details (receipt date, name, text content, type, size, file content, download uri) for original files.

- A client can download an original file.

- A client can delete an original file.



**Backlog**

- RESTful api.

- Implementing Spring Security to work with JWT.

- Unit tests.

- Using Postgres as database (psql -p 5432 "mydatabase").

- Implementing a permanent storage service.

- Centralizing custom exception handling.

- Mapping dto to entity with Mapstruct.

- Using JPA as ORM.

- Async and await following fetch request for simple uploader page using pure JS.

- Using JSP for simple login page.

- Docker as application containerization: 

docker build -t personal-account:latest . | 
docker run -p 8080:8080 personal-account
